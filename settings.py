# -*- coding: utf-8 -*-
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import djcelery
djcelery.setup_loader()


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))
ADMINS = (
    ('ak', 'akoptev1989@yandex.ru'),
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '2tz6)tm=%mbn#=09zv*0pf-@5x^_wq#i2dw%7v0fx)f=njd%hn'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

BROKER_URL = 'redis://localhost:6379/0'

CELERY_SEND_EVENTS=True
CELERY_RESULT_BACKEND='redis'
CELERY_TASK_RESULT_EXPIRES =  10
CELERYBEAT_SCHEDULER="djcelery.schedulers.DatabaseScheduler"
CELERY_ALWAYS_EAGER=False

# Application definition

INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'sorl.thumbnail',
    'south',
    'django_generic_flatblocks',
    'ckeditor',
    'mptt',
    'rosetta',
    'celery_admin',


    #-- my help utils----#
    'utils',
    'forms_builder.forms',
    'utils.gblocks',
    'pagination',
    'easy_thumbnails',
    'widget_tweaks',
    'transmeta',
    'django_extensions',
    'djcelery',
    'djkombu',
    'hvad',
    #'registration',

    #----- my apps ------#
    'plugins',
    'plugins.page',
    'plugins.news',
    'plugins.slider',
    'plugins.catalog',
    'plugins.events',
    'plugins.contacts',
    'plugins.account',
    'plugins.player',
    'plugins.authorization',
    'plugins.propaganda',
    'plugins.seo',

    #'plugins.feincms'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'utils.lang.middleware.AdminLocaleURLMiddleware',
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    "django.core.context_processors.i18n",
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)



ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'vinicio',                      # Or path to database file if using sqlite3.
        'USER': 'docent_user',                      # Not used with sqlite3.
        'PASSWORD': 'Frpsrapef883feee',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        'OPTIONS'  : { 'init_command' : 'SET storage_engine=MyISAM', },
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-Ru'
DATE_INPUT_FORMATS = ('%d.%m.%Y',)
SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = False

TEMPLATE_DIRS = (
    BASE_DIR + '/vinicio/templates',
    BASE_DIR + '/templates',
    BASE_DIR + '/feincms/templates',
    BASE_DIR + '/italian/templates',
    BASE_DIR + '/italian/plugins/catalog/templates',
    BASE_DIR + '/plugins/events/',
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

MEDIA_ROOT = BASE_DIR + "/media"
MEDIA_URL = '/media/'

STATIC_ROOT = MEDIA_ROOT + '/static'
STATIC_URL = '/media/static/'

ADMIN_TOOLS_MENU = 'menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'dashboard.CustomAppIndexDashboard'

SOUTH_MIGRATION_MODULES = {
        'easy_thumbnails': 'easy_thumbnails.south_migrations',
    }
CKEDITOR_UPLOAD_PATH = "filer_public/"
FILER_IS_PUBLIC_DEFAULT = True

AUTH_PROFILE_MODULE = "account.models.UserProfile"

ADMIN_LANGUAGE_CODE = 'ru-Ru'

LANGUAGE_CODE = 'en'
#ugettext = lambda s: s
LANGUAGES = (
    ('en', 'English'),
    ('it', 'Italiano'),
)
LOCALE_PATHS = (
    BASE_DIR + '/vinicio/locale',

)

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'akoptev1989@yandex.ru'
EMAIL_HOST_PASSWORD = 'tuPrHyQEdEkISol-XsRlyA'
EMAIL_USE_TLS = False


DEFAULT_FROM_EMAIL = 'no-reply@viniciocamerlengo.com'


try:
    from local_settings import *
except ImportError:
    pass
