import os
import sys

sys.path.insert(0, '/var/www/viniciocamerlengo.com/vinenv/lib/python2.7/site-packages')
sys.path.insert(0, '/var/www/viniciocamerlengo.com/vinicio/local')
sys.path.insert(0, '/var/www/viniciocamerlengo.com/vinicio')

from django.core.handlers.wsgi import WSGIHandler

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
application = WSGIHandler()
