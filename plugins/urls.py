# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.views.generic.base import TemplateView

urlpatterns = patterns('plugins.views',
    url(r'^$', 'home', name='home'),
    url(r'^intro/$','intro', name='intro'),
    url(r'^page/', include('plugins.page.urls')),
	url(r'^news/', include('plugins.news.urls')),
	url(r'^robots.txt$', 'robots', name='robots'),
	url(r'^catalog/', include('plugins.catalog.urls')),
	url(r'^events/', include('plugins.events.urls')),
	url(r'^contacts/', include('plugins.contacts.urls')),
	url(r'^register/', include('plugins.account.urls')),
	url(r'^player/', include('plugins.player.urls')),
	url(r'^authorization/', include('plugins.authorization.urls')),
)
