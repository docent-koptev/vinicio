# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404,HttpResponse
from django.http import Http404
from django.utils import simplejson
from utils.annoying.decorators import render_to
from utils.mail.utils import mail_feedback
from django.template import RequestContext
from plugins.contacts.models import Contacts
from plugins.contacts.forms import ContactsForm

def show_contacts(request,template_name="contacts/contact_list.html"):
	form = ContactsForm ()
	return render_to_response(template_name, RequestContext(request, {
        "form" : form,
    }))

def send_form(request):
	if request.method == 'POST':
		form = ContactsForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			phone = cd['phone']
			body = cd['text']
			name = cd['name']
			email = cd['email']
			form.save()
			#admin notification
			mail_feedback(body,phone,name,email)
			return HttpResponse(simplejson.dumps({"result":"success"}))
		else:
			errors = {}
			for k in form.errors:
			    # Теоретически у поля может быть несколько ошибок...
			    errors[k] = form.errors[k][0]
			return HttpResponse(simplejson.dumps({"errors":errors,"result":"error"}))
