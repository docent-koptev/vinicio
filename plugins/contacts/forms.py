# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms import ModelForm
from django import forms
from models import Contacts


class ContactsForm(ModelForm):
    class Meta:
		model = Contacts
		exclude = ['date']
		widgets = {
		    'name': forms.TextInput(attrs={'placeholder': _(u'Enter Your Name')}),
		    'email': forms.TextInput(attrs={'placeholder': _(u'Enter Your E-mail')}),
		    'phone': forms.TextInput(attrs={'placeholder': _(u'Enter Your Phone')}),
		    'text': forms.Textarea(attrs={'placeholder': _(u'Enter Your Message')}),
		}