from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.contacts.views',
    url(r'^$', 'show_contacts', name='show_contacts'),
    url(r'^update/$', 'send_form', name='send_form'),


)