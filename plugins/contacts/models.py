# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField
from datetime import datetime    

class Contacts(models.Model):
	name = models.CharField(_(u'Имя'), max_length=255,blank=True,null=True)
	email = models.CharField(_(u'Email'), max_length=255)
	phone = models.CharField(_(u'Телефон'), max_length=255,blank=True,null=True)
	text = models.TextField(_(u'Текст'),blank=True,null=True)
	date = models.DateField(_(u'Дата поступления'),default=datetime.now,blank=True,null=True)



	class Meta:
	    ordering = ('-name',)
	    verbose_name = _(u'Письма и заявки')
	    verbose_name_plural = _(u'Письма и заявки')

	def __unicode__(self):
	    return self.name
