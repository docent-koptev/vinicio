# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import Contacts
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget



class EventAdmin(admin.ModelAdmin):
	list_display = ['name', 'email','phone','text','date']
	list_filter = ("name","email","phone","date",)

admin.site.register(Contacts,EventAdmin)