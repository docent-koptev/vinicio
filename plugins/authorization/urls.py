# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.generic import TemplateView

urlpatterns = patterns('plugins.authorization.views',
    url(r'^$', 'authorization', name='authorization'),
    url(r'^update/$', 'update_reg', name='update_reg'),
)
