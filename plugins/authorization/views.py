# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404
from utils.annoying.decorators import render_to
from django.template import RequestContext
from plugins.authorization.models import Authorization



def authorization(request,template_name="authorization/authorization_list.html"):
	events = Authorization.objects.filter(display=True)
	return render_to_response(template_name, RequestContext(request, {
        "events" : events,
    }))

def update_reg(request,template_name="authorization/update_list.html"):
	if request.method == 'GET' and 'num' in request.GET:
		event = get_object_or_404(Authorization,pk=int(request.GET['num']))
		return render_to_response(template_name,RequestContext(request,{
			"event":event,
		}))
	else:
		return Http404
