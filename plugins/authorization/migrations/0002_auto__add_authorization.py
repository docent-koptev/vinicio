# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Authorization'
        db.create_table(u'authorization_authorization', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('body_en', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('body_it', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('short_en', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('short_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('date_en', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('date_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('place_en', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('place_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('image', self.gf(u'sorl.thumbnail.fields.ImageField')(max_length=100, blank=True)),
            ('pdf', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('display', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('creation_date', self.gf('django.db.models.fields.DateField')(blank=True)),
        ))
        db.send_create_signal(u'authorization', ['Authorization'])


    def backwards(self, orm):
        # Deleting model 'Authorization'
        db.delete_table(u'authorization_authorization')


    models = {
        u'authorization.authorization': {
            'Meta': {'ordering': "('-creation_date',)", 'object_name': 'Authorization'},
            'body_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'body_it': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'date_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'pdf': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'place_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'place_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'short_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'short_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['authorization']