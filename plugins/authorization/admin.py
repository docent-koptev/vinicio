# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import Authorization
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget


class AuthorizationAdminForm(forms.ModelForm):
        body_en = forms.CharField(widget=CKEditorWidget(),label=u'Текст англ.')
        body_it = forms.CharField(widget=CKEditorWidget(),label=u'Текст ит.')
        class Meta:
            model = Authorization

class AuthorizationAdmin(admin.ModelAdmin):
	form = AuthorizationAdminForm
	prepopulated_fields = {'slug': ['title_en',]}
	list_display = ['title_en', 'short','link','date','place','display',]
	list_filter = ("display","title_en","link",)
	fieldsets = (
        ('Заголовок и рабочая ссылка', {
            # 'classes': ('collapse',),
            'fields': (('title_en', 'title_it',),"slug",),
        }),
        ('Укажите полный текст события', {
            # 'classes': ('collapse',),
            'fields': ('body_en',"body_it",),
        }),
        ('Укажите краткое описание событие', {
            # 'classes': ('collapse',),
            'fields': (('short_en',"short_it",),"link",),
        }),
        ('Укажите когда будет происходить события', {
            # 'classes': ('collapse',),
            'fields': (('date_en',"date_it",),),
        }),
        ('Укажите место проведения события', {
            # 'classes': ('collapse',),
            'fields': (('place_en',"place_it",),),
        }),

        ('Укажите изображение собития и вложите PDF-документ', {
            # 'classes': ('collapse',),
            'fields': ('image',"pdf",),
        }),
        ('Управляйте событием', {
            # 'classes': ('collapse',),
            'fields': ("creation_date",'display'),
        }),
    )

admin.site.register(Authorization,AuthorizationAdmin)