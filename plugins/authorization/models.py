# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

from transmeta import TransMeta



class Authorization(models.Model):
	__metaclass__ = TransMeta
	title = models.CharField(_(u'заголовок'), max_length=255,help_text=u'не более 20 символов')
	slug = models.SlugField(unique=True,help_text=u'не меняем.Это для ссылки')
	body = models.TextField(_(u'текст события'), blank=True)
	short = models.CharField(_(u'Краткий текст события'), blank=True,max_length=255,help_text=u'Коротко.Это заголовок')
	link = models.CharField(_(u'Ссылка'), blank=True, max_length=255,help_text=u'ссылка на событие')
	date = models.CharField(_(u'Когда'), blank=True, max_length=255,help_text=u'Пишем текстом.Отображение - справа')
	place = models.CharField(_(u'Место проведения'), blank=True, max_length=255)
	image = ImageField(_(u'изображение'), upload_to='uploads/events', blank=True,help_text=u'фото вставляется справа')
	pdf = models.FileField(_(u'PDF-документ'), upload_to='uploads/pdf', blank=True,null=True)
	creation_date = models.DateField(_(u'дата публикации'), blank=True)
	display = models.BooleanField(_(u'опубликовано'), default=True)


	class Meta:
	    ordering = ('-creation_date',)
	    verbose_name = _(u'Каталог')
	    verbose_name_plural = _(u'Каталоги')
	    translate = ('title','body','short','date','place',)

	def __unicode__(self):
	    return self.title