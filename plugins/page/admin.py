# django imports
# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.db import models
from django.conf import settings

from models import *

from mptt.admin import MPTTModelAdmin as FeinCMSModelAdmin
from ckeditor.widgets import CKEditorWidget


class PageAdminForm(forms.ModelForm):
        body_en = forms.CharField(widget=CKEditorWidget(),label=u'Текст англ.')
        body_it = forms.CharField(widget=CKEditorWidget(),label=u'Текст ит.')
        class Meta:
            model = Page


class PageAdmin(admin.ModelAdmin):
	"""
	"""
	prepopulated_fields = prepopulated_fields = {"slug": ("title_en",)}

	form = PageAdminForm
	
admin.site.register(Page, PageAdmin)

class ActionInline(admin.TabularInline):
	model = Action
	extra = 1

class ActionGroupAdmin(admin.ModelAdmin):
	inlines = [ActionInline,]

admin.site.register(ActionGroup, ActionGroupAdmin)

class ActionAdmin(FeinCMSModelAdmin):
	list_filter = ('group',)
	list_display = ('__unicode__', 'active', 'group',)
	list_editable = ('active', 'group',)

admin.site.register(Action, ActionAdmin)