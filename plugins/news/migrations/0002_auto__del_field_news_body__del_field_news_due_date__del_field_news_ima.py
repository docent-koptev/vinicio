# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'News.body'
        db.delete_column(u'news_news', 'body')

        # Deleting field 'News.due_date'
        db.delete_column(u'news_news', 'due_date')

        # Deleting field 'News.image'
        db.delete_column(u'news_news', 'image')

        # Deleting field 'News.is_share'
        db.delete_column(u'news_news', 'is_share')

        # Deleting field 'News.is_sended'
        db.delete_column(u'news_news', 'is_sended')

        # Deleting field 'News.short'
        db.delete_column(u'news_news', 'short')

        # Deleting field 'News.title'
        db.delete_column(u'news_news', 'title')

        # Deleting field 'News.slug'
        db.delete_column(u'news_news', 'slug')

        # Adding field 'News.title_en'
        db.add_column(u'news_news', 'title_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'News.title_it'
        db.add_column(u'news_news', 'title_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'News.body_en'
        db.add_column(u'news_news', 'body_en',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'News.body_it'
        db.add_column(u'news_news', 'body_it',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'News.body'
        db.add_column(u'news_news', 'body',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'News.due_date'
        db.add_column(u'news_news', 'due_date',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'News.image'
        raise RuntimeError("Cannot reverse this migration. 'News.image' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'News.image'
        db.add_column(u'news_news', 'image',
                      self.gf('sorl.thumbnail.fields.ImageField')(max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'News.is_share'
        db.add_column(u'news_news', 'is_share',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'News.is_sended'
        db.add_column(u'news_news', 'is_sended',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'News.short'
        db.add_column(u'news_news', 'short',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'News.title'
        raise RuntimeError("Cannot reverse this migration. 'News.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'News.title'
        db.add_column(u'news_news', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=255),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'News.slug'
        raise RuntimeError("Cannot reverse this migration. 'News.slug' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'News.slug'
        db.add_column(u'news_news', 'slug',
                      self.gf('django.db.models.fields.SlugField')(max_length=50, unique=True),
                      keep_default=False)

        # Deleting field 'News.title_en'
        db.delete_column(u'news_news', 'title_en')

        # Deleting field 'News.title_it'
        db.delete_column(u'news_news', 'title_it')

        # Deleting field 'News.body_en'
        db.delete_column(u'news_news', 'body_en')

        # Deleting field 'News.body_it'
        db.delete_column(u'news_news', 'body_it')


    models = {
        u'news.news': {
            'Meta': {'ordering': "('-creation_date',)", 'object_name': 'News'},
            'body_en': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'body_it': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_modification': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['news']