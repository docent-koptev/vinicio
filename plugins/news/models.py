# -*- coding: utf-8 -*-
import datetime
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from transmeta import TransMeta
from sorl.thumbnail.fields import ImageField

class News(models.Model):
    __metaclass__ = TransMeta
    title = models.CharField(_(u'заголовок'), max_length=255,blank=True)
    body = models.TextField(_(u'текст новости'), blank=True)
    display = models.BooleanField(_(u'опубликовать'), default=True)
    creation_date = models.DateTimeField(_(u'дата публикации'), blank=True)
    last_modification = models.DateTimeField(_(u'дата последнего изменения'), blank=True)
    pdf = models.FileField(_(u'PDF-документ'), upload_to='uploads/pdf', blank=True,null=True)

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _(u'Новость')
        verbose_name_plural = _(u'Новости')
        translate = ('title','body',)

    def __unicode__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, *args, **kw):
        self.last_modification = datetime.datetime.now()
        if not self.creation_date:
            self.creation_date = datetime.datetime.now()
        super(News, self).save(*args, **kw)


class NewsSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return News.objects.filter(display=True)