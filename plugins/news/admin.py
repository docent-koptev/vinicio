# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import News
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget

class NewsAdminForm(forms.ModelForm):
        body_en = forms.CharField(widget=CKEditorWidget())
        body_it = forms.CharField(widget=CKEditorWidget())
        class Meta:
            model = News

class NewsAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ['title_en',  'display', 'last_modification', 'creation_date']
    list_editable = ['display',]
    search_fields = ['title_en', 'body_en',]
    list_filter = ['display',]
    date_hierarchy = 'creation_date'
    readonly_fields = ['last_modification',]
    fieldsets = (
        ('Заголовок', {
            # 'classes': ('collapse',),
            'fields': (('title_en','title_it',)),
        }),
        ('Описание', {
            # 'classes': ('collapse',),
            'fields': ('body_en',"body_it",),
        }),

        ('Даты и активность', {
            # 'classes': ('collapse',),
            'fields': ('pdf','creation_date',"last_modification","display",),
        }),
    )

    form = NewsAdminForm

admin.site.register(News, NewsAdmin)