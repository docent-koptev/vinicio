# -*- coding: utf-8 -*-
from utils.annoying.decorators import render_to
from plugins.slider.models import Slider
from django.shortcuts import get_object_or_404, redirect, render,render_to_response
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.template import RequestContext
from django.http import HttpResponseRedirect

from django.views.decorators.csrf import requires_csrf_token


@render_to(template='home/home.html')
def home(request):
	if 'django_language' not in request.session:
		return render_to_response("start.html",(),RequestContext(request, {}))
	else: 
		slider = Slider.objects.filter(display=True, show_on='main')
		return locals()

def intro(request):
	return render_to_response("start.html",(),RequestContext(request, {}))
	

def robots(request):
    return render(request, 'robots.txt', {}, content_type='text/plain')


@requires_csrf_token
def server_error(request, template_name='errors/500.html'):
    return render(request, template_name)

@requires_csrf_token
def not_found_error(request, template_name='errors/404.html'):
    return render(request, template_name)

@requires_csrf_token
def permission_denied(request, template_name='errors/403.html'):
    return render(request, template_name)