from django.contrib.auth.models import User
from django.db.models import signals
from django.conf import settings
from utils.mail.utils import user_activate_notify

def pre_user_save(sender, instance, *args, **kwargs):
	try:
		user_pre = User.objects.get(id=instance.id)
		user_active = user_pre.is_active
		if instance.is_active == True and not user_active:
			user_activate_notify(instance)
	except User.DoesNotExist:
		user_active = False
signals.pre_save.connect(pre_user_save, sender=User, dispatch_uid='pre_user_save')