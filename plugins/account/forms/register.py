# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from plugins.account.models import UserProfile
from utils.mail.utils import new_user_register_to_admin
from django.utils.translation import ugettext_lazy as _


class RegistrationForm(forms.ModelForm):
    """
    Form for registering a new account.
    """
    username = forms.CharField(error_messages={'invalid': _(u'Username is incorrectly. Gaps are forbidden')},widget=forms.TextInput(attrs={'placeholder': _(u'Enter Your Username')}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': _(u'Enter Your E-mail')}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _(u'Enter Your Phone')}))
    country = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _(u'Enter Your Country')}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _(u'Enter Your Password')}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _(u'Enter Your Confirmation')}))
    is_subscribe = forms.BooleanField(label=_(u'If you want to subscribe to the newsletter, put a check'),required=False)
    class Meta:
        model = User
        fields = ['username','email', 'phone', 'country','password1', 'password2']

    def clean(self):
        """
        Verifies that the values entered into the password fields match

        NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
        """
        cleaned_data = super(RegistrationForm, self).clean()
    

        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_(u"Passwords don't match. Please enter both fields again."))
        if 'email' in self.cleaned_data:
            email = self.cleaned_data['email']
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError(_(u"This email already used"))
        if 'phone' in self.cleaned_data:
            phone = self.cleaned_data['phone']
            if UserProfile.objects.filter(phone=phone).exists():
                raise forms.ValidationError(_(u"This phone already used"))
        return self.cleaned_data

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        if user.is_active:
            user.is_active = False
            user.save()

        profile = UserProfile(user=user,email=self.cleaned_data['email'],phone=self.cleaned_data['phone'],country=self.cleaned_data['country'],is_subscribe=self.cleaned_data['is_subscribe'],password=self.cleaned_data['password1'])
        profile.save()
        
        #send notify for admin
        new_user_register_to_admin(user)


        return user