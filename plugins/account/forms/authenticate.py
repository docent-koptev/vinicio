from django import forms
from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

class AuthenticationForm(forms.Form):
	"""
	Login form
	"""
	username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _(u'Enter Your Username')}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': _(u'Enter Your Password')}))

	def clean(self):
	    """
	    Verifies that the values entered into the password fields match

	    NOTE: Errors here will appear in ``non_field_errors()`` because it applies to more than one field.
	    """
	    cleaned_data = super(AuthenticationForm, self).clean()
	    if 'username' in self.cleaned_data:
	        username = self.cleaned_data['username']
	        if not User.objects.filter(username=username).exists():
	        	self.cleaned_data['username'] = None
	        	raise forms.ValidationError(_(u"User with this name do not exist or user don't active yet"))
	    		
	    return self.cleaned_data
	class Meta:
	    fields = ['username', 'password']