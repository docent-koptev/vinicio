# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, redirect,HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth import login as django_login, authenticate, logout as django_logout

from forms import  RegistrationForm,AuthenticationForm

def login_and_register(request, template_name="account/login_register_form.html"):
	register_form = RegistrationForm()
	login_form = AuthenticationForm()
	if request.POST.get("action") == "register":
	    register_form = RegistrationForm(data=request.POST)
	    if register_form.is_valid():
	        user = register_form.save()	        
	        return render_to_response('account/thanks.html',{},RequestContext(request))
	    else:
	    	register_form = RegistrationForm(data=request.POST)

	elif request.POST.get("action") == "login":
		login_form = AuthenticationForm(data=request.POST)
		if login_form.is_valid():
		    user = authenticate(username=request.POST['username'], password=request.POST['password'])
		    if user is not None:
		        if user.is_active:
		            django_login(request, user)
		            return redirect('authorization')
		else:
			login_form = AuthenticationForm(data=request.POST)

	return render_to_response(template_name, RequestContext(request, {
		'register_form':register_form,
		'login_form':login_form,
    }))



