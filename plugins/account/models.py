# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import User



"""class User(AbstractBaseUser):
	username = models.CharField(verbose_name=u'Имя',  max_length=255, unique=True)
	email = models.EmailField(u'Почта', unique=True)
	phone = models.CharField(verbose_name=u'Телефон',  max_length=255, unique=True)
	country = models.CharField(verbose_name=u'Страна',  max_length=255)
	is_subscribe = models.BooleanField(default=False,verbose_name=u'Получает рассылки')
	is_active = models.BooleanField(default=False,verbose_name=u'Активен')
	is_admin = models.BooleanField(default=False)

	USERNAME_FIELD = 'username'

	def __unicode__(self):
		return self.email

	class Meta:
		verbose_name = u'Пользователь'
		verbose_name_plural = u'Пользователи сайта'
"""

class UserProfile(models.Model):
	user = models.ForeignKey(User, unique=True)
	email = models.EmailField(u'Почта', unique=True)
	phone = models.CharField(verbose_name=u'Телефон',  max_length=255, unique=True)
	country = models.CharField(verbose_name=u'Страна',  max_length=255)
	password = models.CharField(verbose_name=u'Пароль',  max_length=255,null=True)
	is_subscribe = models.BooleanField(default=False,verbose_name=u'Получает рассылки')
	
	def __unicode__(self):
	    return self.user.username

	class Meta:
		verbose_name = u'Профиль'
		verbose_name_plural = u'Профили'
		
