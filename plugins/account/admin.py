# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.db import models
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User
from plugins.account.models import UserProfile


class UserProfileInline(admin.StackedInline):
	model = UserProfile
	max_num = 1
	can_delete = False

class UserAdmin(admin.ModelAdmin):
	inlines = [UserProfileInline]
	list_display = ("username",'email','is_active')
	exclude = ("groups","password","user_permissions")

	fieldsets = (
        ('Общая информация', {
            # 'classes': ('collapse',),
            'fields': ('username', 'email',),
        }),
        ('Управление', {
            # 'classes': ('collapse',),
            'fields': ('is_active',),
        }),
    )
admin.site.unregister(Group)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)