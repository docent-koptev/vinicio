# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.generic import TemplateView

urlpatterns = patterns('plugins.account.views',
    url(r'^$', 'login_and_register', name='login_and_register'),
)

# logout
urlpatterns += patterns('django.contrib.auth.views',
     url(r'^logout/$', "logout", {'next_page': '/'}, name='logout'),
)