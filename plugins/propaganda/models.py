# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

from plugins.account.models import UserProfile

from transmeta import TransMeta



class Propaganda(models.Model):
	users = models.ManyToManyField(UserProfile,verbose_name=_(u"Кому"),limit_choices_to = {'is_subscribe':True,'user__is_active':True})
	title = models.CharField(_(u'Название рассылки'), max_length=255,help_text=u"Отобразится в теме письма")
	body = models.TextField(_(u'текст рассылки'), blank=True,help_text=u"Отобразится как тело письма")
	pdf = models.FileField(_(u'PDF-документ'), upload_to='uploads/pdf', blank=True,null=True,help_text=u"Как вложение.Например,каталог")
	display = models.BooleanField(_(u'Активно'), default=True)
	creation_date = models.DateField(_(u'дата публикации'), blank=True)
	is_done = models.BooleanField(_(u'Рассылка прошла'), default=False,help_text=u"Контроль за рассылкой")

	class Meta:
	    ordering = ('-creation_date',)
	    verbose_name = _(u'Рассылка')
	    verbose_name_plural = _(u'Рассылки')

	def __unicode__(self):
	    return self.title
	        