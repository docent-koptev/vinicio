# -*- coding: utf-8 -*-
from django.contrib import admin
from plugins.propaganda.models import Propaganda
from ckeditor.widgets import CKEditorWidget
from django import forms


class PropagandaForm(forms.ModelForm):
        body = forms.CharField(widget=CKEditorWidget(),label=u'Текст англ.')
        class Meta:
            model = Propaganda

class PropagandaAdmin(admin.ModelAdmin):
	list_display = ('title','body','display','creation_date','is_done')
	filter_horizontal = ('users',)
	readonly_fields = ('is_done',)
	form = PropagandaForm
	save_on_top = True
	fieldsets = (
        ('Укажите название рассылки,текст', {
            'fields': ('title', 'body',),
        }),

        ('Управляйте Вашей рассылкой', {
            'fields': ('users',"creation_date",'display',"is_done"),
        }),
    )
	

admin.site.register(Propaganda,PropagandaAdmin)