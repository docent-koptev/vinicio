# -*- coding: utf-8 -*-
from celery.task import periodic_task
from datetime import timedelta

from plugins.propaganda.models import Propaganda
from utils.mail.utils import propaganda_mail_send
from django.core.mail import send_mail

from celery.utils.log import get_task_logger
logger = get_task_logger(__name__)

@periodic_task(run_every = timedelta(seconds = 60))
def propaganda():
	Prop_list = Propaganda.objects.filter(display=True,is_done=False)
	if Prop_list:
		status = False
		for mailer in Prop_list:
			for user in mailer.users.all():
				if propaganda_mail_send(user.user,mailer.title,user.user.email,mailer.body):
					status = True
				else:
					status = False


