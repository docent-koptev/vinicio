# -*- coding: utf-8 -*
from django.db import models
from transmeta import TransMeta

class Seo(models.Model):
	__metaclass__ = TransMeta
	name = models.CharField(max_length=255,verbose_name=u"Что переводим",help_text=u"указываем путь.Например,/contacts/")
	title = models.CharField(max_length=255,verbose_name=u"Тайтл")
	description = models.CharField(max_length=255,verbose_name=u"Описание",blank=True,null=True)
	keywords = models.CharField(max_length=255,verbose_name=u"Ключевые слова",blank=True,null=True)
    
	class Meta:
		    verbose_name = u'СЕО модуль'
		    verbose_name_plural = u'СЕО'
		    translate = ('title','description','keywords',)

	def __unicode__(self):
	    return self.name