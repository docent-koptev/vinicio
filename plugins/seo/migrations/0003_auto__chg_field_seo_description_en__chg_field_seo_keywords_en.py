# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Seo.description_en'
        db.alter_column(u'seo_seo', 'description_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Seo.keywords_en'
        db.alter_column(u'seo_seo', 'keywords_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Seo.description_en'
        raise RuntimeError("Cannot reverse this migration. 'Seo.description_en' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Seo.description_en'
        db.alter_column(u'seo_seo', 'description_en', self.gf('django.db.models.fields.CharField')(max_length=255))

        # User chose to not deal with backwards NULL issues for 'Seo.keywords_en'
        raise RuntimeError("Cannot reverse this migration. 'Seo.keywords_en' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Seo.keywords_en'
        db.alter_column(u'seo_seo', 'keywords_en', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'seo.seo': {
            'Meta': {'object_name': 'Seo'},
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'description_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'keywords_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['seo']