# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Seo'
        db.create_table(u'seo_seo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('description_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('keywords_en', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('keywords_it', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'seo', ['Seo'])


    def backwards(self, orm):
        # Deleting model 'Seo'
        db.delete_table(u'seo_seo')


    models = {
        u'seo.seo': {
            'Meta': {'object_name': 'Seo'},
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'description_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'keywords_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'title_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['seo']