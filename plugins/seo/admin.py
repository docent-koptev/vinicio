# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Seo

class SeoAdmin(admin.ModelAdmin):
	list_display = ('name','title_en','description_en',"keywords_en")
	fieldsets = (
        (None, {
            # 'classes': ('collapse',),
            'fields': ('name',),
        }),
        ('Управление заголовками, описанием, ключевыми словами', {
            # 'classes': ('collapse',),
            'fields': (('title_en','title_it'),('description_en','description_it'),('keywords_en','keywords_it'),),
        }),
    )
admin.site.register(Seo,SeoAdmin)