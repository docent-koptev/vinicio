# -*- coding: utf-8 -*-
from django.template import Node, TemplateSyntaxError
from django import template
from django.template.loader import render_to_string

from datetime import datetime
from django import template
from utils.annoying.functions import get_first_or_None
from plugins.seo.models import Seo

register = template.Library()

@register.inclusion_tag('seo/seo.html', takes_context=True)
def docent_seo(context):
	request = context.get("request")
	path = request.path
	seo_meta = None
	if path:
		seo_meta = get_first_or_None(Seo,name__startswith=path)
	context.update({'seo_meta':seo_meta,})
	return context