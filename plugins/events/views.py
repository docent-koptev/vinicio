# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404
from utils.annoying.decorators import render_to
from django.template import RequestContext
from plugins.events.models import Event
from plugins.news.models import News



def events(request,template_name="events/event_list.html"):
	events = Event.objects.filter(display=True)
	news = News.objects.filter(display=True).order_by("-creation_date")[:5]
	return render_to_response(template_name, RequestContext(request, {
        "events" : events,
        "news" : news,
    }))

