# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import Event
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget

class EventAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ['title_en',]}
	list_display = ['title_en','image_img','link','date','place','display',]
	fieldsets = (
        ('Контактная информация', {
            # 'classes': ('collapse',),
            'fields': (('title_en', 'title_it',),"slug","link",("date_en","date_it",),("place_en","place_it",)),
        }),
        ('Управление', {
            # 'classes': ('collapse',),
            'fields': ('image',"pdf","creation_date","display",),
        }),
    )

admin.site.register(Event,EventAdmin)