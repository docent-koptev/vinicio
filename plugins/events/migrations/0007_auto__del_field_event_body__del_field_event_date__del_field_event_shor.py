# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Event.body'
        db.delete_column(u'events_event', 'body')

        # Deleting field 'Event.date'
        db.delete_column(u'events_event', 'date')

        # Deleting field 'Event.short'
        db.delete_column(u'events_event', 'short')

        # Deleting field 'Event.title'
        db.delete_column(u'events_event', 'title')

        # Deleting field 'Event.place'
        db.delete_column(u'events_event', 'place')

        # Adding field 'Event.title_en'
        db.add_column(u'events_event', 'title_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_it'
        db.add_column(u'events_event', 'title_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.date_en'
        db.add_column(u'events_event', 'date_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Event.date_it'
        db.add_column(u'events_event', 'date_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.place_en'
        db.add_column(u'events_event', 'place_en',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Event.place_it'
        db.add_column(u'events_event', 'place_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Event.body'
        db.add_column(u'events_event', 'body',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'Event.date'
        db.add_column(u'events_event', 'date',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Event.short'
        db.add_column(u'events_event', 'short',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Event.title'
        raise RuntimeError("Cannot reverse this migration. 'Event.title' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Event.title'
        db.add_column(u'events_event', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=255),
                      keep_default=False)

        # Adding field 'Event.place'
        db.add_column(u'events_event', 'place',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)

        # Deleting field 'Event.title_en'
        db.delete_column(u'events_event', 'title_en')

        # Deleting field 'Event.title_it'
        db.delete_column(u'events_event', 'title_it')

        # Deleting field 'Event.date_en'
        db.delete_column(u'events_event', 'date_en')

        # Deleting field 'Event.date_it'
        db.delete_column(u'events_event', 'date_it')

        # Deleting field 'Event.place_en'
        db.delete_column(u'events_event', 'place_en')

        # Deleting field 'Event.place_it'
        db.delete_column(u'events_event', 'place_it')


    models = {
        u'events.event': {
            'Meta': {'ordering': "('-creation_date',)", 'object_name': 'Event'},
            'creation_date': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'date_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'date_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'pdf': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'place_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'place_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'title_it': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['events']