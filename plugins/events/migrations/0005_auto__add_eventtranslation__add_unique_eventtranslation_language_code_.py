# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EventTranslation'
        db.create_table(u'events_event_translation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.TextField')()),
            ('body', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('language_code', self.gf('django.db.models.fields.CharField')(max_length=15, db_index=True)),
            ('master', self.gf('django.db.models.fields.related.ForeignKey')(related_name='translations', null=True, to=orm['events.Event'])),
        ))
        db.send_create_signal(u'events', ['EventTranslation'])

        # Adding unique constraint on 'EventTranslation', fields ['language_code', 'master']
        db.create_unique(u'events_event_translation', ['language_code', 'master_id'])

        # Deleting field 'Event.short_en'
        db.delete_column(u'events_event', 'short_en')

        # Deleting field 'Event.body_it'
        db.delete_column(u'events_event', 'body_it')

        # Deleting field 'Event.place_en'
        db.delete_column(u'events_event', 'place_en')

        # Deleting field 'Event.short_it'
        db.delete_column(u'events_event', 'short_it')

        # Deleting field 'Event.title_en'
        db.delete_column(u'events_event', 'title_en')

        # Deleting field 'Event.title_it'
        db.delete_column(u'events_event', 'title_it')

        # Deleting field 'Event.place_it'
        db.delete_column(u'events_event', 'place_it')

        # Deleting field 'Event.body_en'
        db.delete_column(u'events_event', 'body_en')


    def backwards(self, orm):
        # Removing unique constraint on 'EventTranslation', fields ['language_code', 'master']
        db.delete_unique(u'events_event_translation', ['language_code', 'master_id'])

        # Deleting model 'EventTranslation'
        db.delete_table(u'events_event_translation')

        # Adding field 'Event.short_en'
        db.add_column(u'events_event', 'short_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.body_it'
        db.add_column(u'events_event', 'body_it',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.place_en'
        db.add_column(u'events_event', 'place_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.short_it'
        db.add_column(u'events_event', 'short_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_en'
        db.add_column(u'events_event', 'title_en',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.title_it'
        db.add_column(u'events_event', 'title_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.place_it'
        db.add_column(u'events_event', 'place_it',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Event.body_en'
        db.add_column(u'events_event', 'body_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    models = {
        u'events.event': {
            'Meta': {'ordering': "('-creation_date',)", 'object_name': 'Event'},
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'date': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': (u'sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'pdf': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'short': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'events.eventtranslation': {
            'Meta': {'unique_together': "[('language_code', 'master')]", 'object_name': 'EventTranslation', 'db_table': "u'events_event_translation'"},
            'body': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'master': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': u"orm['events.Event']"}),
            'title': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['events']