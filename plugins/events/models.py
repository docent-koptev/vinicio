# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

from transmeta import TransMeta



class Event(models.Model):
	__metaclass__ = TransMeta
	title = models.CharField(_(u'заголовок'), max_length=255,blank=True)
	slug = models.SlugField(unique=True)
	#body = models.TextField(_(u'текст события'), blank=True)
	#short = models.CharField(_(u'Краткий текст события'), blank=True,max_length=255)
	link = models.CharField(_(u'Ссылка'), blank=True, max_length=255)
	date = models.CharField(_(u'Когда'), blank=True, max_length=255)
	place = models.CharField(_(u'Место проведения'), blank=True, max_length=255)
	image = ImageField(_(u'изображение'), upload_to='uploads/events', blank=True)
	pdf = models.FileField(_(u'PDF-документ'), upload_to='uploads/pdf', blank=True,null=True)
	display = models.BooleanField(_(u'опубликовано'), default=True)
	creation_date = models.DateField(_(u'дата публикации'), blank=True)

	class Meta:
	    ordering = ('-creation_date',)
	    verbose_name = _(u'Событие')
	    verbose_name_plural = _(u'События')
	    translate = ('title','date','place',)

	def __unicode__(self):
	    return self.title

	def image_img(self):
		if self.image:
			return u'<img style="margin-left:20px;" src="%s" width="50"/>' % self.image.url
		else:
			return None

	image_img.short_description = u'Изображение'
	image_img.allow_tags = True
