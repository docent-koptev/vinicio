from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.events.views',
    url(r'^$', 'events', name='events'),
)