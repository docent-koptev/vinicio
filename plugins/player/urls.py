from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.player.views',
    url(r'^list/$', 'get_list', name='plist'),
)