# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404,HttpResponse
from django.http import Http404
from utils.annoying.decorators import render_to
from django.template import RequestContext
from plugins.player.models import Player
from django.utils import simplejson as json


def get_list(request):
	if request.method == 'GET' and 'data' in request.GET:
		plist = []
		play_list = Player.objects.filter(active=True)
		for music in play_list:
			plist.append(music.sound.name)
		return HttpResponse(json.dumps(plist), mimetype="application/json" )