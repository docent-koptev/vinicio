# -*- coding: utf-8 -*-
from django.contrib import admin
from plugins.player.models import Player

class PlayerAdmin(admin.ModelAdmin):
	list_display = ['name', 'sound','active',]


admin.site.register(Player,PlayerAdmin)
