# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

class Player(models.Model):
    name = models.CharField(_(u'Название композиции'), max_length = 127,help_text=u'не выводится.Только для админа')
    sound = models.FileField(_(u'Мелодия'), upload_to="upload/music/",help_text=u'mp3, mp4 (AAC/H.264), ogg (Vorbis/Theora), webm (Vorbis/VP8), wav')
    active = models.BooleanField(_(u'Активность'),default=False)
    

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _(u'Мелодия')
        verbose_name_plural = _(u'Мелодии')
