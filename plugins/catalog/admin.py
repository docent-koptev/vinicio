# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.db import models
from django.conf import settings

from plugins.catalog.models import Product

class ProductAdmin(admin.ModelAdmin):
	list_display = ("image_img","name","active",)
	list_filter = ("name","active")
	search_fields = ("name",)

admin.site.register(Product,ProductAdmin)