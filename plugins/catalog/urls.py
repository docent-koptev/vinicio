# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from views import generic_categories


urlpatterns = patterns('',
    url(r'^$', generic_categories,name='show_catalog'),
)

