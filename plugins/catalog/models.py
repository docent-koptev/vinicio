# -*- coding: utf-8 -*-

from django.db import models
from sorl.thumbnail import ImageField, get_thumbnail
from django.utils.translation import ugettext_lazy as _

class Product(models.Model):
	name = models.CharField(_(u'Название'), max_length = 127)
	active = models.BooleanField(_(u'Активность'),default=False)
	image = models.ImageField(_(u'Image'), blank=True, default=None, null=True, upload_to="catalog/product/")

	class Meta:
		ordering = ['name']
		verbose_name = _(u'Продукт')
		verbose_name_plural = _(u'Продукты')
	
	def __unicode__(self):
		return self.name



	def image_img(self):
		if self.image:
			return u'<img style="margin-left:20px;" src="%s" width="50"/>' % self.image.url
		else:
			return None

	image_img.short_description = u'Изображение'
	image_img.allow_tags = True



