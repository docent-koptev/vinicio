# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.http import Http404
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from utils.annoying.decorators import render_to
from django.template import RequestContext
from plugins.catalog.models import Product



def generic_categories(request,template_name="catalog/category.html"):
    if request.user.is_authenticated():
        product_list = Product.objects.filter(active=True)
    else:
        product_list = Product.objects.filter(active=True)[:20]
    paginator = Paginator(product_list, 16)
    page = request.GET.get('page')

    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    return render_to_response(template_name, RequestContext(request, {
        "products" : products,
    }))