-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: vinicio
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_preferences_dashboard_id_575b1104_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_6340c63c` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{}','events-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_6340c63c` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add kv store',8,'add_kvstore'),(23,'Can change kv store',8,'change_kvstore'),(24,'Can delete kv store',8,'delete_kvstore'),(25,'Can add migration history',9,'add_migrationhistory'),(26,'Can change migration history',9,'change_migrationhistory'),(27,'Can delete migration history',9,'delete_migrationhistory'),(28,'Can add generic flatblock',10,'add_genericflatblock'),(29,'Can change generic flatblock',10,'change_genericflatblock'),(30,'Can delete generic flatblock',10,'delete_genericflatblock'),(31,'Can add title',11,'add_title'),(32,'Can change title',11,'change_title'),(33,'Can delete title',11,'delete_title'),(34,'Can add text',12,'add_text'),(35,'Can change text',12,'change_text'),(36,'Can delete text',12,'delete_text'),(37,'Can add image',13,'add_image'),(38,'Can change image',13,'change_image'),(39,'Can delete image',13,'delete_image'),(40,'Can add title and text',14,'add_titleandtext'),(41,'Can change title and text',14,'change_titleandtext'),(42,'Can delete title and text',14,'delete_titleandtext'),(43,'Can add title text and image',15,'add_titletextandimage'),(44,'Can change title text and image',15,'change_titletextandimage'),(45,'Can delete title text and image',15,'delete_titletextandimage'),(46,'Can add banner',16,'add_banner'),(47,'Can change banner',16,'change_banner'),(48,'Can delete banner',16,'delete_banner'),(49,'Can add SEO (Path)',17,'add_seometadatapath'),(50,'Can change SEO (Path)',17,'change_seometadatapath'),(51,'Can delete SEO (Path)',17,'delete_seometadatapath'),(52,'Can add SEO (Model Instance)',18,'add_seometadatamodelinstance'),(53,'Can change SEO (Model Instance)',18,'change_seometadatamodelinstance'),(54,'Can delete SEO (Model Instance)',18,'delete_seometadatamodelinstance'),(55,'Can add SEO (Model)',19,'add_seometadatamodel'),(56,'Can change SEO (Model)',19,'change_seometadatamodel'),(57,'Can delete SEO (Model)',19,'delete_seometadatamodel'),(58,'Can add SEO (View)',20,'add_seometadataview'),(59,'Can change SEO (View)',20,'change_seometadataview'),(60,'Can delete SEO (View)',20,'delete_seometadataview'),(61,'Can add bookmark',25,'add_bookmark'),(62,'Can change bookmark',25,'change_bookmark'),(63,'Can delete bookmark',25,'delete_bookmark'),(64,'Can add dashboard preferences',26,'add_dashboardpreferences'),(65,'Can change dashboard preferences',26,'change_dashboardpreferences'),(66,'Can delete dashboard preferences',26,'delete_dashboardpreferences'),(67,'Can add Form entry',27,'add_formentry'),(68,'Can change Form entry',27,'change_formentry'),(69,'Can delete Form entry',27,'delete_formentry'),(70,'Can add Form field entry',28,'add_fieldentry'),(71,'Can change Form field entry',28,'change_fieldentry'),(72,'Can delete Form field entry',28,'delete_fieldentry'),(73,'Can add Form',29,'add_form'),(74,'Can change Form',29,'change_form'),(75,'Can delete Form',29,'delete_form'),(76,'Can add Field',30,'add_field'),(77,'Can change Field',30,'change_field'),(78,'Can delete Field',30,'delete_field'),(79,'Can add source',31,'add_source'),(80,'Can change source',31,'change_source'),(81,'Can delete source',31,'delete_source'),(82,'Can add thumbnail',32,'add_thumbnail'),(83,'Can change thumbnail',32,'change_thumbnail'),(84,'Can delete thumbnail',32,'delete_thumbnail'),(85,'Can add thumbnail dimensions',33,'add_thumbnaildimensions'),(86,'Can change thumbnail dimensions',33,'change_thumbnaildimensions'),(87,'Can delete thumbnail dimensions',33,'delete_thumbnaildimensions'),(88,'Can add Меню',21,'add_actiongroup'),(89,'Can change Меню',21,'change_actiongroup'),(90,'Can delete Меню',21,'delete_actiongroup'),(91,'Can add Элемент меню',22,'add_action'),(92,'Can change Элемент меню',22,'change_action'),(93,'Can delete Элемент меню',22,'delete_action'),(94,'Can add текстовая страница',23,'add_page'),(95,'Can change текстовая страница',23,'change_page'),(96,'Can delete текстовая страница',23,'delete_page'),(97,'Can add Новость',24,'add_news'),(98,'Can change Новость',24,'change_news'),(99,'Can delete Новость',24,'delete_news'),(100,'Can add слайд',34,'add_slider'),(101,'Can change слайд',34,'change_slider'),(102,'Can delete слайд',34,'delete_slider'),(103,'Can add Продукт',35,'add_product'),(104,'Can change Продукт',35,'change_product'),(105,'Can delete Продукт',35,'delete_product'),(106,'Can add Событие',36,'add_event'),(107,'Can change Событие',36,'change_event'),(108,'Can delete Событие',36,'delete_event'),(109,'Can add Письма и заявки',37,'add_contacts'),(110,'Can change Письма и заявки',37,'change_contacts'),(111,'Can delete Письма и заявки',37,'delete_contacts');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$VaS05rJNdcu5$NLSS41ecWZIsgwWtqMT54eWDcvzUm1OgUZn0lsRLhJM=','2014-07-17 18:38:09',1,'docent','','','akoptev1989@yandex.ru',1,1,'2014-07-17 18:29:19');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_product`
--

DROP TABLE IF EXISTS `catalog_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_product`
--

LOCK TABLES `catalog_product` WRITE;
/*!40000 ALTER TABLE `catalog_product` DISABLE KEYS */;
INSERT INTO `catalog_product` VALUES (1,'Первый',1,'catalog/product/1.png'),(2,'Второй',1,'catalog/product/2.png'),(3,'Третий',1,'catalog/product/3.png');
/*!40000 ALTER TABLE `catalog_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_contacts`
--

DROP TABLE IF EXISTS `contacts_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `text` longtext,
  `date` date,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_contacts`
--

LOCK TABLES `contacts_contacts` WRITE;
/*!40000 ALTER TABLE `contacts_contacts` DISABLE KEYS */;
INSERT INTO `contacts_contacts` VALUES (1,'test','akoptev1989@yandex.ru','test','test','2014-07-17'),(2,'','test','','','2014-07-17'),(3,'','test@test.ru','','','2014-07-17'),(4,'','test@test.ru','','','2014-07-17'),(5,'','test','','','2014-07-17'),(6,'','test@test.ru','','','2014-07-17'),(7,'','test@test.ru','','','2014-07-17'),(8,'','test@test.ru','','','2014-07-17'),(9,'test','test@test.ru','test','test','2014-07-17'),(10,'test','test@test.ru','test','test','2014-07-17'),(11,'test','akoptev1989@yandex.ru','test','','2014-07-17'),(12,'','test@test.ru','','','2014-07-17'),(13,'','test@test.ru','','','2014-07-17'),(14,'','akoptev1989@yandex.ru','','','2014-07-17'),(15,'','test@test.ru','','','2014-07-18'),(16,'','test@test.ru','','','2014-07-18'),(17,'','akoptev1989@yandex.ru','','','2014-07-18'),(18,'','test@test.ru','','','2014-07-18');
/*!40000 ALTER TABLE `contacts_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-07-17 18:38:26',1,35,'1','Первый',1,''),(2,'2014-07-17 18:38:34',1,35,'2','Второй',1,''),(3,'2014-07-17 18:38:45',1,35,'3','Третий',1,''),(4,'2014-07-17 19:20:51',1,21,'1','main_menu',1,''),(5,'2014-07-17 19:20:59',1,21,'1','main_menu',2,'Добавлен Элемент меню \"main_menu - \". Изменены active для Элемент меню \"main_menu - home\". Изменены active для Элемент меню \"main_menu - about us\". Изменены active для Элемент меню \"main_menu - catalogue\". Изменены active для Элемент меню \"main_menu - events\". Изменены active для Элемент меню \"main_menu - contacts\". Изменены active для Элемент меню \"main_menu - authorization\".'),(6,'2014-07-17 19:21:25',1,21,'1','main_menu',2,'Изменены link для Элемент меню \"main_menu - home\".'),(7,'2014-07-17 19:21:54',1,23,'1','about us',1,''),(8,'2014-07-17 19:21:56',1,21,'1','main_menu',2,'Изменены page для Элемент меню \"main_menu - about us\".'),(9,'2014-07-17 19:22:20',1,34,'1','main: первый',1,''),(10,'2014-07-17 19:22:29',1,34,'2','main: второй',1,''),(11,'2014-07-17 19:37:23',1,36,'1','Первое событие',1,''),(12,'2014-07-17 19:49:02',1,36,'2','Второе событие',1,''),(13,'2014-07-17 20:15:41',1,36,'3','третье событие',1,''),(14,'2014-07-17 20:19:08',1,36,'1','Первое событие',2,'Изменен body.'),(15,'2014-07-17 20:22:52',1,36,'1','Первое событие',2,'Изменен body и pdf.'),(16,'2014-07-17 20:40:41',1,21,'1','main_menu',2,'Изменены link для Элемент меню \"main_menu - events\".'),(17,'2014-07-17 21:02:44',1,21,'1','main_menu',2,'Изменены link для Элемент меню \"main_menu - contacts\".'),(18,'2014-07-17 21:03:12',1,21,'1','main_menu',2,'Изменены link для Элемент меню \"main_menu - contacts\".'),(19,'2014-07-17 21:08:39',1,14,'1','(TitleAndTextBlock) Контакты',2,'Изменен title и text.');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'kv store','thumbnail','kvstore'),(9,'migration history','south','migrationhistory'),(10,'generic flatblock','django_generic_flatblocks','genericflatblock'),(11,'title','gblocks','title'),(12,'text','gblocks','text'),(13,'image','gblocks','image'),(14,'title and text','gblocks','titleandtext'),(15,'title text and image','gblocks','titletextandimage'),(16,'banner','gblocks','banner'),(17,'SEO (Path)','seo','seometadatapath'),(18,'SEO (Model Instance)','seo','seometadatamodelinstance'),(19,'SEO (Model)','seo','seometadatamodel'),(20,'SEO (View)','seo','seometadataview'),(21,'Меню','page','actiongroup'),(22,'Элемент меню','page','action'),(23,'текстовая страница','page','page'),(24,'Новость','news','news'),(25,'bookmark','menu','bookmark'),(26,'dashboard preferences','dashboard','dashboardpreferences'),(27,'Form entry','forms','formentry'),(28,'Form field entry','forms','fieldentry'),(29,'Form','forms','form'),(30,'Field','forms','field'),(31,'source','easy_thumbnails','source'),(32,'thumbnail','easy_thumbnails','thumbnail'),(33,'thumbnail dimensions','easy_thumbnails','thumbnaildimensions'),(34,'слайд','slider','slider'),(35,'Продукт','catalog','product'),(36,'Событие','events','event'),(37,'Письма и заявки','contacts','contacts');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_generic_flatblocks_genericflatblock`
--

DROP TABLE IF EXISTS `django_generic_flatblocks_genericflatblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_generic_flatblocks_genericflatblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `django_generic_flatblocks_genericflatblock_37ef4eb4` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_generic_flatblocks_genericflatblock`
--

LOCK TABLES `django_generic_flatblocks_genericflatblock` WRITE;
/*!40000 ALTER TABLE `django_generic_flatblocks_genericflatblock` DISABLE KEYS */;
INSERT INTO `django_generic_flatblocks_genericflatblock` VALUES (1,'info',14,1);
/*!40000 ALTER TABLE `django_generic_flatblocks_genericflatblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('j8f3wedgyl8c8c3kyvsbiipkprrfe8ta','MGNlZmYyNDE1ZGNkODk3YmQ1ZGJmYmNlNTRiMDdhMTkzNzUwZTg2OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MSwiX21lc3NhZ2VzIjoiW1tcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWZcXHUwNDQwXFx1MDQzZVxcdTA0MzRcXHUwNDQzXFx1MDQzYVxcdTA0NDIgXFxcIlxcdTA0MWZcXHUwNDM1XFx1MDQ0MFxcdTA0MzJcXHUwNDRiXFx1MDQzOVxcXCIgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC4gXFx1MDQxZFxcdTA0MzhcXHUwNDM2XFx1MDQzNSBcXHUwNDMyXFx1MDQ0YiBcXHUwNDNjXFx1MDQzZVxcdTA0MzZcXHUwNDM1XFx1MDQ0MlxcdTA0MzUgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDM4XFx1MDQ0MlxcdTA0NGMgXFx1MDQzNVxcdTA0NDlcXHUwNDM1IFxcdTA0MWZcXHUwNDQwXFx1MDQzZVxcdTA0MzRcXHUwNDQzXFx1MDQzYVxcdTA0NDIuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWZcXHUwNDQwXFx1MDQzZVxcdTA0MzRcXHUwNDQzXFx1MDQzYVxcdTA0NDIgXFxcIlxcdTA0MTJcXHUwNDQyXFx1MDQzZVxcdTA0NDBcXHUwNDNlXFx1MDQzOVxcXCIgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC4gXFx1MDQxZFxcdTA0MzhcXHUwNDM2XFx1MDQzNSBcXHUwNDMyXFx1MDQ0YiBcXHUwNDNjXFx1MDQzZVxcdTA0MzZcXHUwNDM1XFx1MDQ0MlxcdTA0MzUgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDM4XFx1MDQ0MlxcdTA0NGMgXFx1MDQzNVxcdTA0NDlcXHUwNDM1IFxcdTA0MWZcXHUwNDQwXFx1MDQzZVxcdTA0MzRcXHUwNDQzXFx1MDQzYVxcdTA0NDIuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWZcXHUwNDQwXFx1MDQzZVxcdTA0MzRcXHUwNDQzXFx1MDQzYVxcdTA0NDIgXFxcIlxcdTA0MjJcXHUwNDQwXFx1MDQzNVxcdTA0NDJcXHUwNDM4XFx1MDQzOVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQzZFxcdTA0NGUgXFxcIm1haW5fbWVudVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuIFxcdTA0MWRcXHUwNDM4XFx1MDQzNlxcdTA0MzUgXFx1MDQzMlxcdTA0NGIgXFx1MDQzY1xcdTA0M2VcXHUwNDM2XFx1MDQzNVxcdTA0NDJcXHUwNDM1IFxcdTA0NDFcXHUwNDNkXFx1MDQzZVxcdTA0MzJcXHUwNDMwIFxcdTA0MzVcXHUwNDMzXFx1MDQzZSBcXHUwNDNlXFx1MDQ0MlxcdTA0NDBcXHUwNDM1XFx1MDQzNFxcdTA0MzBcXHUwNDNhXFx1MDQ0MlxcdTA0MzhcXHUwNDQwXFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0NGMuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQzZFxcdTA0NGUgXFxcIm1haW5fbWVudVxcXCIgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLiBcXHUwNDFkXFx1MDQzOFxcdTA0MzZcXHUwNDM1IFxcdTA0MzJcXHUwNDRiIFxcdTA0M2NcXHUwNDNlXFx1MDQzNlxcdTA0MzVcXHUwNDQyXFx1MDQzNSBcXHUwNDQwXFx1MDQzNVxcdTA0MzRcXHUwNDMwXFx1MDQzYVxcdTA0NDJcXHUwNDM4XFx1MDQ0MFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDRjIFxcdTA0NDFcXHUwNDNkXFx1MDQzZVxcdTA0MzJcXHUwNDMwLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0M2RcXHUwNDRlIFxcXCJtYWluX21lbnVcXFwiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC4gXFx1MDQxZFxcdTA0MzhcXHUwNDM2XFx1MDQzNSBcXHUwNDMyXFx1MDQ0YiBcXHUwNDNjXFx1MDQzZVxcdTA0MzZcXHUwNDM1XFx1MDQ0MlxcdTA0MzUgXFx1MDQ0MFxcdTA0MzVcXHUwNDM0XFx1MDQzMFxcdTA0M2FcXHUwNDQyXFx1MDQzOFxcdTA0NDBcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQ0YyBcXHUwNDQxXFx1MDQzZFxcdTA0M2VcXHUwNDMyXFx1MDQzMC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDNkXFx1MDQ0ZSBcXFwibWFpbl9tZW51XFxcIiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuIFxcdTA0MWRcXHUwNDM4XFx1MDQzNlxcdTA0MzUgXFx1MDQzMlxcdTA0NGIgXFx1MDQzY1xcdTA0M2VcXHUwNDM2XFx1MDQzNVxcdTA0NDJcXHUwNDM1IFxcdTA0NDBcXHUwNDM1XFx1MDQzNFxcdTA0MzBcXHUwNDNhXFx1MDQ0MlxcdTA0MzhcXHUwNDQwXFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0NGMgXFx1MDQ0MVxcdTA0M2RcXHUwNDNlXFx1MDQzMlxcdTA0MzAuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0NDFcXHUwNDNiXFx1MDQzMFxcdTA0MzlcXHUwNDM0IFxcXCJtYWluOiBcXHUwNDNmXFx1MDQzNVxcdTA0NDBcXHUwNDMyXFx1MDQ0YlxcdTA0MzlcXFwiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuIFxcdTA0MWRcXHUwNDM4XFx1MDQzNlxcdTA0MzUgXFx1MDQzMlxcdTA0NGIgXFx1MDQzY1xcdTA0M2VcXHUwNDM2XFx1MDQzNVxcdTA0NDJcXHUwNDM1IFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzOFxcdTA0NDJcXHUwNDRjIFxcdTA0MzVcXHUwNDQ5XFx1MDQzNSBcXHUwNDQxXFx1MDQzYlxcdTA0MzBcXHUwNDM5XFx1MDQzNC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQ0MVxcdTA0M2JcXHUwNDMwXFx1MDQzOVxcdTA0MzQgXFxcIm1haW46IFxcdTA0MzJcXHUwNDQyXFx1MDQzZVxcdTA0NDBcXHUwNDNlXFx1MDQzOVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MjFcXHUwNDNlXFx1MDQzMVxcdTA0NGJcXHUwNDQyXFx1MDQzOFxcdTA0MzUgXFxcIlxcdTA0MWZcXHUwNDM1XFx1MDQ0MFxcdTA0MzJcXHUwNDNlXFx1MDQzNSBcXHUwNDQxXFx1MDQzZVxcdTA0MzFcXHUwNDRiXFx1MDQ0MlxcdTA0MzhcXHUwNDM1XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQyMVxcdTA0M2VcXHUwNDMxXFx1MDQ0YlxcdTA0NDJcXHUwNDM4XFx1MDQzNSBcXFwiXFx1MDQxMlxcdTA0NDJcXHUwNDNlXFx1MDQ0MFxcdTA0M2VcXHUwNDM1IFxcdTA0NDFcXHUwNDNlXFx1MDQzMVxcdTA0NGJcXHUwNDQyXFx1MDQzOFxcdTA0MzVcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDIxXFx1MDQzZVxcdTA0MzFcXHUwNDRiXFx1MDQ0MlxcdTA0MzhcXHUwNDM1IFxcXCJcXHUwNDQyXFx1MDQ0MFxcdTA0MzVcXHUwNDQyXFx1MDQ0Y1xcdTA0MzUgXFx1MDQ0MVxcdTA0M2VcXHUwNDMxXFx1MDQ0YlxcdTA0NDJcXHUwNDM4XFx1MDQzNVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MjFcXHUwNDNlXFx1MDQzMVxcdTA0NGJcXHUwNDQyXFx1MDQzOFxcdTA0MzUgXFxcIlxcdTA0MWZcXHUwNDM1XFx1MDQ0MFxcdTA0MzJcXHUwNDNlXFx1MDQzNSBcXHUwNDQxXFx1MDQzZVxcdTA0MzFcXHUwNDRiXFx1MDQ0MlxcdTA0MzhcXHUwNDM1XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDIxXFx1MDQzZVxcdTA0MzFcXHUwNDRiXFx1MDQ0MlxcdTA0MzhcXHUwNDM1IFxcXCJcXHUwNDFmXFx1MDQzNVxcdTA0NDBcXHUwNDMyXFx1MDQzZVxcdTA0MzUgXFx1MDQ0MVxcdTA0M2VcXHUwNDMxXFx1MDQ0YlxcdTA0NDJcXHUwNDM4XFx1MDQzNVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDNkXFx1MDQ0ZSBcXFwibWFpbl9tZW51XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0M2RcXHUwNDRlIFxcXCJtYWluX21lbnVcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQzZFxcdTA0NGUgXFxcIm1haW5fbWVudVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwidGl0bGUgYW5kIHRleHQgXFxcIihUaXRsZUFuZFRleHRCbG9jaykgXFx1MDQxYVxcdTA0M2VcXHUwNDNkXFx1MDQ0MlxcdTA0MzBcXHUwNDNhXFx1MDQ0MlxcdTA0NGJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdXSJ9','2014-07-31 21:08:39');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easy_thumbnails_source`
--

DROP TABLE IF EXISTS `easy_thumbnails_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `storage_hash` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `easy_thumbnails_source_name_39229697_uniq` (`name`,`storage_hash`),
  KEY `easy_thumbnails_source_4da47e07` (`name`),
  KEY `easy_thumbnails_source_3f0464e5` (`storage_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easy_thumbnails_source`
--

LOCK TABLES `easy_thumbnails_source` WRITE;
/*!40000 ALTER TABLE `easy_thumbnails_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `easy_thumbnails_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easy_thumbnails_thumbnail`
--

DROP TABLE IF EXISTS `easy_thumbnails_thumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `source_id` int(11) NOT NULL,
  `storage_hash` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `easy_thumbnails_thumbnail_source_id_47e6eb80_uniq` (`source_id`,`name`,`storage_hash`),
  KEY `easy_thumbnails_thumbnail_a34b03a6` (`source_id`),
  KEY `easy_thumbnails_thumbnail_4da47e07` (`name`),
  KEY `easy_thumbnails_thumbnail_3f0464e5` (`storage_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easy_thumbnails_thumbnail`
--

LOCK TABLES `easy_thumbnails_thumbnail` WRITE;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnail` DISABLE KEYS */;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easy_thumbnails_thumbnaildimensions`
--

DROP TABLE IF EXISTS `easy_thumbnails_thumbnaildimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_thumbnaildimensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail_id` int(11) NOT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `thumbnail_id` (`thumbnail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easy_thumbnails_thumbnaildimensions`
--

LOCK TABLES `easy_thumbnails_thumbnaildimensions` WRITE;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnaildimensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnaildimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_event`
--

DROP TABLE IF EXISTS `events_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `body` longtext NOT NULL,
  `short` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `creation_date` date NOT NULL,
  `pdf` varchar(100),
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_event`
--

LOCK TABLES `events_event` WRITE;
/*!40000 ALTER TABLE `events_event` DISABLE KEYS */;
INSERT INTO `events_event` VALUES (1,'Первое событие','pervoe-sobytie','<p>The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that</p>\r\n\r\n<p>The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means thatThe tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that</p>\r\n','The forebear of Vinicio Camerlengo brand is seignior Vinicio Camerlengo.','www.consum-expo.ru','21-23 SEPTEMBER 2012','crokus','uploads/events/40.png',1,'2014-07-17','uploads/pdf/AngularJSIn60MinutesIsh_DanWahlin_May2013_1.pdf'),(2,'Второе событие','vtoroe-sobytie','The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that\r\nThe tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that\r\nThe tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that\r\nThe tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of\r\nThe tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that\r\nThe tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that\r\nThe tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that\r\nThe tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of\r\nThe tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern It','The forebear of Vinicio Camerlengo brand is seignior Vinicio Camerlengo','http://gpt.ru2/','21-23 SEPTEMBER 2012','crokus','uploads/events/40_1.png',1,'2014-07-17',NULL),(3,'третье событие','trete-sobytie','<p>The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that The tradition of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that question of The tradition www.ffany.org of the Italian handicraftsmen to give their own name to the product they produce had passed to modern Italian industrialists. This Italian tradition means that</p>\r\n','The forebear of Vinicio Camerlengo brand is seignior Vinicio Camerlengo','http://gp2u/','21-23 SEPTEMBER 2012','crokus','uploads/events/40_2.png',1,'2014-07-17','uploads/pdf/AngularJSIn60MinutesIsh_DanWahlin_May2013.pdf');
/*!40000 ALTER TABLE `events_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_field`
--

DROP TABLE IF EXISTS `forms_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(200) NOT NULL,
  `field_type` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `choices` varchar(1000) NOT NULL,
  `default` varchar(2000) NOT NULL,
  `placeholder_text` varchar(100),
  `help_text` varchar(100) NOT NULL,
  `form_id` int(11) NOT NULL,
  `order` int(11),
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forms_field_c3d79a6c` (`form_id`),
  KEY `forms_field_f52cfca0` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_field`
--

LOCK TABLES `forms_field` WRITE;
/*!40000 ALTER TABLE `forms_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_fieldentry`
--

DROP TABLE IF EXISTS `forms_fieldentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_fieldentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `value` varchar(2000),
  `entry_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forms_fieldentry_e8d920b6` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_fieldentry`
--

LOCK TABLES `forms_fieldentry` WRITE;
/*!40000 ALTER TABLE `forms_fieldentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_fieldentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_form`
--

DROP TABLE IF EXISTS `forms_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `intro` longtext NOT NULL,
  `button_text` varchar(50) NOT NULL,
  `response` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `publish_date` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `login_required` tinyint(1) NOT NULL,
  `send_email` tinyint(1) NOT NULL,
  `email_from` varchar(75) NOT NULL,
  `email_copies` varchar(200) NOT NULL,
  `email_subject` varchar(200) NOT NULL,
  `email_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_form`
--

LOCK TABLES `forms_form` WRITE;
/*!40000 ALTER TABLE `forms_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_form_sites`
--

DROP TABLE IF EXISTS `forms_form_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_form_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forms_form_sites_form_id_66edaea_uniq` (`form_id`,`site_id`),
  KEY `forms_form_sites_c3d79a6c` (`form_id`),
  KEY `forms_form_sites_99732b5c` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_form_sites`
--

LOCK TABLES `forms_form_sites` WRITE;
/*!40000 ALTER TABLE `forms_form_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_form_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_formentry`
--

DROP TABLE IF EXISTS `forms_formentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_formentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_time` datetime NOT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forms_formentry_c3d79a6c` (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_formentry`
--

LOCK TABLES `forms_formentry` WRITE;
/*!40000 ALTER TABLE `forms_formentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_formentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_banner`
--

DROP TABLE IF EXISTS `gblocks_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_banner`
--

LOCK TABLES `gblocks_banner` WRITE;
/*!40000 ALTER TABLE `gblocks_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_image`
--

DROP TABLE IF EXISTS `gblocks_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_image`
--

LOCK TABLES `gblocks_image` WRITE;
/*!40000 ALTER TABLE `gblocks_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_text`
--

DROP TABLE IF EXISTS `gblocks_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_text`
--

LOCK TABLES `gblocks_text` WRITE;
/*!40000 ALTER TABLE `gblocks_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_title`
--

DROP TABLE IF EXISTS `gblocks_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_title`
--

LOCK TABLES `gblocks_title` WRITE;
/*!40000 ALTER TABLE `gblocks_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_titleandtext`
--

DROP TABLE IF EXISTS `gblocks_titleandtext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_titleandtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_titleandtext`
--

LOCK TABLES `gblocks_titleandtext` WRITE;
/*!40000 ALTER TABLE `gblocks_titleandtext` DISABLE KEYS */;
INSERT INTO `gblocks_titleandtext` VALUES (1,'Контакты','<h5>The manufacturer and the Vinicio Camerlengo brand owner is the Italian company Commercom Italia.</h5>\r\n\r\n							<p>Via Purita, 126, CAP 62015,</p>\r\n							<p>Monte san Giusto (MC), Italia</p>\r\n							<p>tel./fax: +39–0733–83–70-34</p>\r\n							<p>cel.: +39–335-606-00-45, +39-328-969-56-61</p>\r\n							<p>e-mail:<a href=\"mailto:info@viniciocamerlengo.i\"> info@viniciocamerlengo.i</a>t</p>\r\n							<p>Skype: vinicio_camerlengo</p>\r\n							<p>ICQ: VINICIO_CAMERLENGO (600887740)</p>\r\n							<p>website: <a href=\"www.viniciocamerlengo.com\">www.viniciocamerlengo.com</a></p>');
/*!40000 ALTER TABLE `gblocks_titleandtext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_titletextandimage`
--

DROP TABLE IF EXISTS `gblocks_titletextandimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_titletextandimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_titletextandimage`
--

LOCK TABLES `gblocks_titletextandimage` WRITE;
/*!40000 ALTER TABLE `gblocks_titletextandimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_titletextandimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_news`
--

DROP TABLE IF EXISTS `news_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `body` longtext NOT NULL,
  `short` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `is_share` tinyint(1) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `last_modification` datetime NOT NULL,
  `is_sended` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_news`
--

LOCK TABLES `news_news` WRITE;
/*!40000 ALTER TABLE `news_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_action`
--

DROP TABLE IF EXISTS `page_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `link` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_action_5f412f9a` (`group_id`),
  KEY `page_action_410d0aac` (`parent_id`),
  KEY `page_action_3fb9c94f` (`page_id`),
  KEY `page_action_329f6fb3` (`lft`),
  KEY `page_action_e763210f` (`rght`),
  KEY `page_action_ba470c4a` (`tree_id`),
  KEY `page_action_20e079f4` (`level`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_action`
--

LOCK TABLES `page_action` WRITE;
/*!40000 ALTER TABLE `page_action` DISABLE KEYS */;
INSERT INTO `page_action` VALUES (1,1,'home','reverse__[\"home\", [], {}]',1,999,NULL,NULL,1,2,1,0),(2,1,'about us','/page/about-us',1,999,NULL,1,1,2,2,0),(3,1,'catalogue','reverse__[\"show_catalog\", [], {}]',1,999,NULL,NULL,1,2,3,0),(4,1,'events','reverse__[\"events\", [], {}]',1,999,NULL,NULL,1,2,4,0),(5,1,'contacts','reverse__[\"show_contacts\", [], {}]',1,999,NULL,NULL,1,2,5,0),(6,1,'authorization','',1,999,NULL,NULL,1,2,6,0),(7,1,'','',1,999,NULL,NULL,1,2,7,0);
/*!40000 ALTER TABLE `page_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_actiongroup`
--

DROP TABLE IF EXISTS `page_actiongroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_actiongroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_actiongroup`
--

LOCK TABLES `page_actiongroup` WRITE;
/*!40000 ALTER TABLE `page_actiongroup` DISABLE KEYS */;
INSERT INTO `page_actiongroup` VALUES (1,'main_menu');
/*!40000 ALTER TABLE `page_actiongroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_page`
--

DROP TABLE IF EXISTS `page_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `short_text` longtext NOT NULL,
  `body` longtext NOT NULL,
  `active` tinyint(1) NOT NULL,
  `exclude_from_navigation` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_page`
--

LOCK TABLES `page_page` WRITE;
/*!40000 ALTER TABLE `page_page` DISABLE KEYS */;
INSERT INTO `page_page` VALUES (1,'about us','about-us','<p>test</p>\r\n','<p>test</p>\r\n',1,0,999,'');
/*!40000 ALTER TABLE `page_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadatamodel`
--

DROP TABLE IF EXISTS `seo_seometadatamodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadatamodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_content_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_content_type_id` (`_content_type_id`),
  KEY `seo_seometadatamodel_d674e98a` (`_content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadatamodel`
--

LOCK TABLES `seo_seometadatamodel` WRITE;
/*!40000 ALTER TABLE `seo_seometadatamodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_seometadatamodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadatamodelinstance`
--

DROP TABLE IF EXISTS `seo_seometadatamodelinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadatamodelinstance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_path` varchar(255) NOT NULL,
  `_content_type_id` int(11) NOT NULL,
  `_object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_path` (`_path`),
  UNIQUE KEY `_path_2` (`_path`),
  UNIQUE KEY `_content_type_id` (`_content_type_id`,`_object_id`),
  KEY `seo_seometadatamodelinstance_d674e98a` (`_content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadatamodelinstance`
--

LOCK TABLES `seo_seometadatamodelinstance` WRITE;
/*!40000 ALTER TABLE `seo_seometadatamodelinstance` DISABLE KEYS */;
INSERT INTO `seo_seometadatamodelinstance` VALUES (1,'about us','about us','about us','/page/about-us',23,1);
/*!40000 ALTER TABLE `seo_seometadatamodelinstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadatapath`
--

DROP TABLE IF EXISTS `seo_seometadatapath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadatapath` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_path` (`_path`),
  UNIQUE KEY `_path_2` (`_path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadatapath`
--

LOCK TABLES `seo_seometadatapath` WRITE;
/*!40000 ALTER TABLE `seo_seometadatapath` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_seometadatapath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadataview`
--

DROP TABLE IF EXISTS `seo_seometadataview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadataview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_view` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_view` (`_view`),
  UNIQUE KEY `_view_2` (`_view`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadataview`
--

LOCK TABLES `seo_seometadataview` WRITE;
/*!40000 ALTER TABLE `seo_seometadataview` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_seometadataview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_slider`
--

DROP TABLE IF EXISTS `slider_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `show_on` varchar(250) NOT NULL,
  `image` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `url` varchar(10000) NOT NULL,
  `target` tinyint(1) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_slider`
--

LOCK TABLES `slider_slider` WRITE;
/*!40000 ALTER TABLE `slider_slider` DISABLE KEYS */;
INSERT INTO `slider_slider` VALUES (1,'первый','main','slider/1.jpg','','',0,1,100),(2,'второй','main','slider/2_1.png','','',0,1,100);
/*!40000 ALTER TABLE `slider_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2014-07-17 18:29:27'),(2,'dashboard','0001_initial','2014-07-17 18:29:27'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2014-07-17 18:29:27'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2014-07-17 18:29:27'),(5,'forms','0001_initial','2014-07-17 18:29:28'),(6,'forms','0002_auto__add_field_field_order','2014-07-17 18:29:28'),(7,'forms','0003_auto__add_field_field_slug','2014-07-17 18:29:29'),(8,'forms','0003_auto__chg_field_fieldentry_value','2014-07-17 18:29:29'),(9,'forms','0004_populate_field_slug','2014-07-17 18:29:29'),(10,'forms','0005_auto__chg_field_fieldentry_value__del_field_field__order__chg_field_fi','2014-07-17 18:29:29'),(11,'forms','0006_auto__del_unique_field_slug_form','2014-07-17 18:29:29'),(12,'easy_thumbnails','0001_initial','2014-07-17 18:29:30'),(13,'easy_thumbnails','0002_filename_indexes','2014-07-17 18:29:30'),(14,'easy_thumbnails','0003_auto__add_storagenew','2014-07-17 18:29:30'),(15,'easy_thumbnails','0004_auto__add_field_source_storage_new__add_field_thumbnail_storage_new','2014-07-17 18:29:30'),(16,'easy_thumbnails','0005_storage_fks_null','2014-07-17 18:29:31'),(17,'easy_thumbnails','0006_copy_storage','2014-07-17 18:29:31'),(18,'easy_thumbnails','0007_storagenew_fks_not_null','2014-07-17 18:29:31'),(19,'easy_thumbnails','0008_auto__del_field_source_storage__del_field_thumbnail_storage','2014-07-17 18:29:31'),(20,'easy_thumbnails','0009_auto__del_storage','2014-07-17 18:29:31'),(21,'easy_thumbnails','0010_rename_storage','2014-07-17 18:29:32'),(22,'easy_thumbnails','0011_auto__add_field_source_storage_hash__add_field_thumbnail_storage_hash','2014-07-17 18:29:32'),(23,'easy_thumbnails','0012_build_storage_hashes','2014-07-17 18:29:32'),(24,'easy_thumbnails','0013_auto__del_storage__del_field_source_storage__del_field_thumbnail_stora','2014-07-17 18:29:32'),(25,'easy_thumbnails','0014_auto__add_unique_source_name_storage_hash__add_unique_thumbnail_name_s','2014-07-17 18:29:32'),(26,'easy_thumbnails','0015_auto__del_unique_thumbnail_name_storage_hash__add_unique_thumbnail_sou','2014-07-17 18:29:32'),(27,'easy_thumbnails','0016_auto__add_thumbnaildimensions','2014-07-17 18:29:33'),(28,'page','0001_initial','2014-07-17 18:29:33'),(29,'news','0001_initial','2014-07-17 18:29:33'),(30,'slider','0001_initial','2014-07-17 18:29:33'),(31,'catalog','0001_initial','2014-07-17 18:36:21'),(32,'catalog','0002_auto__add_product','2014-07-17 18:36:39'),(33,'events','0001_initial','2014-07-17 19:25:09'),(34,'events','0002_auto__add_event','2014-07-17 19:32:42'),(35,'events','0003_auto__add_field_event_pdf','2014-07-17 20:02:10'),(36,'contacts','0001_initial','2014-07-17 20:48:57'),(37,'contacts','0002_auto__add_contacts','2014-07-17 20:55:10'),(38,'contacts','0003_auto__add_field_contacts_date','2014-07-17 20:56:56');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
INSERT INTO `thumbnail_kvstore` VALUES ('sorl-thumbnail||image||bf50423e5f50cb978f3ec9b8ff9bd8cd','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"catalog/product/2.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||8e880a50231ab10200dd9afaf9865118','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/82/3f/823fc7db62d3e8e6edc1c5be23028c73.png\", \"size\": [135, 108]}'),('sorl-thumbnail||thumbnails||bf50423e5f50cb978f3ec9b8ff9bd8cd','[\"8e880a50231ab10200dd9afaf9865118\"]'),('sorl-thumbnail||image||f0e0fa622f0fbaf77aef5a87e23ccafc','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"catalog/product/1.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||d9d1c7fa68d21e042573f0bb6a87ded8','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/c7/b8/c7b872507b052b319b3a3ebbefac9b43.png\", \"size\": [135, 108]}'),('sorl-thumbnail||thumbnails||f0e0fa622f0fbaf77aef5a87e23ccafc','[\"d9d1c7fa68d21e042573f0bb6a87ded8\"]'),('sorl-thumbnail||image||f41a2c7bd116601b423235a387da0921','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"catalog/product/3.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||aa64b5f1efc834192e2bca9eb21ccf02','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/1b/be/1bbecc782025d0fac383711a9b57fd79.png\", \"size\": [135, 108]}'),('sorl-thumbnail||thumbnails||f41a2c7bd116601b423235a387da0921','[\"aa64b5f1efc834192e2bca9eb21ccf02\"]'),('sorl-thumbnail||image||cb5ee1a70c909ea6fb05b3960656b1c9','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/32/27/3227a6a4f2b46d279b21f00618ed28bc.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||44ab3f3f91d84da6464cc7df3f01440d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/90/a9/90a9b3e222fa480e6c33b91e0b766f8b.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||608a417ecc6ee2f17a9267782efb8279','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/85/f4/85f41ab5a530db06541737d56745ff85.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||85c8368b7bb09677a80909d4c21716ab','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"slider/1.jpg\", \"size\": [501, 398]}'),('sorl-thumbnail||image||9e073f9f3ff3cf4606ca6fcedf1b86c9','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/da/33/da339af88db6d1a6ea7300414276bea2.png\", \"size\": [501, 398]}'),('sorl-thumbnail||thumbnails||85c8368b7bb09677a80909d4c21716ab','[\"9e073f9f3ff3cf4606ca6fcedf1b86c9\"]'),('sorl-thumbnail||image||c5e0f9110e1dbaf605b1c3d6293a025d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"slider/2_1.png\", \"size\": [500, 400]}'),('sorl-thumbnail||image||6359d43c478a5e2313d9dc5dd1ba8dc2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a7/b3/a7b35722b993559a1e92c947f7f96a4e.png\", \"size\": [500, 400]}'),('sorl-thumbnail||thumbnails||c5e0f9110e1dbaf605b1c3d6293a025d','[\"6359d43c478a5e2313d9dc5dd1ba8dc2\"]'),('sorl-thumbnail||image||e100f08d7ce70a1a1b1d5711b8b2c5df','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/events/40.png\", \"size\": [205, 72]}'),('sorl-thumbnail||image||6156eb5294886b6ccceac15d1cdf6767','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/58/e1/58e17b3459f3a36f7d4b467e75d8fa52.png\", \"size\": [159, 56]}'),('sorl-thumbnail||thumbnails||e100f08d7ce70a1a1b1d5711b8b2c5df','[\"6156eb5294886b6ccceac15d1cdf6767\"]'),('sorl-thumbnail||image||f98ef6b0687b89b72d4004b4990f3d8f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/events/40_1.png\", \"size\": [205, 72]}'),('sorl-thumbnail||image||f9148e5496af4ddd5a2b5d67bd5baebc','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a7/a5/a7a5fbcfa2a3f75b2b4bfac5d5ff152f.png\", \"size\": [159, 56]}'),('sorl-thumbnail||thumbnails||f98ef6b0687b89b72d4004b4990f3d8f','[\"f9148e5496af4ddd5a2b5d67bd5baebc\"]'),('sorl-thumbnail||image||bbb2249db8442a5bbd26d02e1d577ec0','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/events/40_2.png\", \"size\": [205, 72]}'),('sorl-thumbnail||image||9056f7a52c1347d6e76bbbca33b84db6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/88/28/8828d75a7867c0d64beb7d467439c2b9.png\", \"size\": [159, 56]}'),('sorl-thumbnail||thumbnails||bbb2249db8442a5bbd26d02e1d577ec0','[\"9056f7a52c1347d6e76bbbca33b84db6\"]');
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-18  9:05:36
