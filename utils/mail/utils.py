# -*- coding: utf-8 -*-
# django imports
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from plugins.account.models import UserProfile



def mail_feedback(body='',phone='',name='',email=''):
	subject = u'С сайта viniciocamerlengo.com новое сообщение'
	admin = User.objects.get(is_superuser=True)
	from_email = settings.DEFAULT_FROM_EMAIL
	to = [admin.email]

	# text
	text = render_to_string("mail/feedback.txt", {"body" : body,'phone':phone,'name':name,'email':email})
	mail = EmailMultiAlternatives(
	    subject=subject, body=text, from_email=from_email, to=to)

	# html
	html = render_to_string("mail/feedback.html", {
	    "body" : body,'phone':phone,'name':name,'email':email
	})

	mail.attach_alternative(html, "text/html")
	mail.send()


def new_user_register_to_admin(user):
	current_site = Site.objects.get_current()
	subject = u'С сайта viniciocamerlengo.com новый пользователь'
	admin = User.objects.filter(is_superuser=True)
	from_email = settings.DEFAULT_FROM_EMAIL
	for mail in admin:
		to = [mail.email]

		# text
		text = render_to_string("mail/new_user_register_to_admin.txt", {'user':user,'domain':current_site.domain,})
		mail = EmailMultiAlternatives(
		    subject=subject, body=text, from_email=from_email, to=to)

		# html
		html = render_to_string("mail/new_user_register_to_admin.html", {
		    "user":user,'domain':current_site.domain,
		})

		mail.attach_alternative(html, "text/html")
		mail.send()


def user_activate_notify(user):
	current_site = Site.objects.get_current()
	subject = _(u'Your account on a site viniciocamerlengo.com is activated')
	from_email = settings.DEFAULT_FROM_EMAIL
	to = [user.email]
	user_profile = UserProfile.objects.get(user=user)

	# text
	text = render_to_string("mail/new_user_activate.txt", {'user':user,'domain':current_site.domain,'user_profile':user_profile,})
	mail = EmailMultiAlternatives(
	    subject=subject, body=text, from_email=from_email, to=to)

	# html
	html = render_to_string("mail/new_user_activate.html", {
	    "user":user,'domain':current_site.domain,'user_profile':user_profile,
	})

	mail.attach_alternative(html, "text/html")
	mail.send()


def propaganda_mail_send(user,subject='',email='',body=''):
	current_site = Site.objects.get_current()
	from_email = settings.DEFAULT_FROM_EMAIL
	to = [email]

	# text
	text = render_to_string("mail/propaganda.txt", {"user":user,'domain':current_site.domain,'body':body,})
	mail = EmailMultiAlternatives(
	    subject=subject, body=text, from_email=from_email, to=to)

	# html
	html = render_to_string("mail/propaganda.html", {"user":user,'domain':current_site.domain,'body':body,})

	mail.attach_alternative(html, "text/html")
	if mail.send():
		return True
	else:
		return False



def test():
	from django.core.mail import send_mail
	my_list = [1,2,3,4,5,6,7,]
	for p in my_list:
		send_mail('Subject here', 'Here is the message.', 'from@place-start.ru',['akoptev1989@gmail.com'], fail_silently=False)