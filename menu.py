# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django_generic_flatblocks.models import GenericFlatblock

from admin_tools.menu import items, Menu


def get_gblock(slug):
    try:
        return GenericFlatblock.objects.get(slug=slug).content_object
    except Exception as ex:
        return None

def get_gblock_admin_url(slug):
    related_object = get_gblock(slug)
    if related_object:
        app_label = related_object._meta.app_label
        module_name = related_object._meta.module_name
        return '/admin/%s/%s/%s/' % (app_label, module_name, related_object.pk)
    else:
        return '#'


class CustomMenu(Menu):
    """
    Custom Menu for docent admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),            
            items.ModelList(
                u'Сайт',
                models=('page.models.ActionGroup','page.models.Action', 'page.models.Page', 'news.models.News', 'ecko.slider.models.Slider',),
                children = [
                    items.MenuItem(u'Редактируемые блоки',
                        children = [
                            items.MenuItem(
                                u'ссылка для instagram',
                                get_gblock_admin_url('instagram_link'),
                            ),
                            items.MenuItem(
                                u'ссылка для vk.com',
                                get_gblock_admin_url('vk_link'),
                            ),
                            items.MenuItem(
                                u'ссылка для twitter.com',
                                get_gblock_admin_url('tweet_link'),
                            ),
                            items.MenuItem(
                                u'ссылка для lj',
                                get_gblock_admin_url('lj_link'),
                            ),
                            
                            items.MenuItem(
                                u'ссылка для Pinterest',
                                get_gblock_admin_url('pinterest_link'),
                            ),
                            items.MenuItem(
                                u'copyright',
                                get_gblock_admin_url('copyright'),
                            ),

                            items.MenuItem(
                                u'контактная информация',
                                get_gblock_admin_url('info'),
                            ),
                            items.MenuItem(
                                u'Метрика',
                                get_gblock_admin_url('metrika'),
                            ),
    
                        ]
                    ),
                ]
            ),
            items.ModelList(
                u'SEO',
                ('rollyourown.seo.*', ),

            ),

            items.ModelList(
                u'Настройка рассылки',
                ('djcelery.*', ),
            ),
            
        ]


    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
